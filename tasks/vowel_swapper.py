def vowel_swapper(string):
    # ==============
    # Your code here

    list_of = list(string)
    new_list_of = []
    for i in list_of:
        lower_case = i.lower()
        if lower_case == "a":
            new_list_of.append("4")
        elif lower_case == "e":
            new_list_of.append("3")
        elif lower_case == "i":
            new_list_of.append("!")
        elif i == "o":
            new_list_of.append("ooo")
        elif i == "O":
            new_list_of.append("000")
        elif lower_case == "u":
            new_list_of.append("|_|")
        else:
            new_list_of.append(i)
    return(''.join(new_list_of))


    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console