def vowel_swapper(string):
    # ==============
    # Your code here

    list_of = list(string)
    new_list_of = []
    counter = [0,0,0,0,0]
    for i in list_of:
        appended = False
        lower_case = i.lower()
        if lower_case == "a":
            counter[0] = counter[0] + 1
            if counter[0] == 2:
                new_list_of.append("4")
                appended = True

        elif lower_case == "e":
            counter[1] = counter[1] + 1
            if counter[1] == 2:
                new_list_of.append("3")
                appended = True
        elif lower_case == "i":
            counter[2] = counter[2] + 1
            if counter[2] ==2:
                new_list_of.append("!")
                appended = True
        elif i == "o":
            counter[3] = counter[3] + 1
            if counter[3] == 2:
                new_list_of.append("ooo")
                appended = True
        elif i == "O":
            counter[3] = counter[3] + 1
            if counter[3] == 2:
                new_list_of.append("000")
                appended = True
        elif lower_case == "u":
            counter[4] = counter[4] + 1
            if counter[4] == 2:
                new_list_of.append("|_|")
                appended = True
        if appended ==  False:
            new_list_of.append(i)
    return(''.join(new_list_of))


    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
